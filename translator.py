dict_16 = {
    '0': '0', '1': '1', '2': '2', '3': '3',
    '4': '4', '5': '5', '6': '6', '7': '7',
    '8': '8', '9': '9', '10': 'A', '11': 'B',
    '12': 'C', '13': 'D', '14': 'E', '15': 'F'
    }

notation = {
    '2': 2,
    '8': 8,
    '10': 10,
    '16': 16
    }


def func_exchange_2(number: str, str_n: str) -> str:
    '''binary translation function'''
    if number == '0':
        return '0'
    list_n: list = str_n.split('->')
    result: int = 0
    for index, value in enumerate(number[::-1]):
        result += int(value)*2**index
    if list_n[1] == '8':
        num_int = result
        result: str = ''
        while num_int > 0:
            result += str(num_int % 8)
            num_int = num_int // 8
        return str(result[::-1])
    elif list_n[1] == '16':
        num_int = result
        result: str = ''
        while num_int > 0:
            result += dict_16[str(num_int % 16)]
            num_int = num_int // 16
        return str(result[::-1])
    return str(result)


def func_exchange(number: str, str_n: str) -> str:
    '''number translation function'''
    list_n: list = str_n.split('->')
    if number == '0':
        return '0'
    num_int = int(number, notation[list_n[0]])
    result: str = ''
    if notation[list_n[1]] == 16:
        while num_int > 0:
            result += dict_16[str(num_int % 16)]
            num_int: int = num_int // 16
    else:
        while num_int > 0:
            result += str(num_int % notation[list_n[1]])
            num_int = num_int // notation[list_n[1]]
    return result[::-1]
