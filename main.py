import translator
import telebot

from telebot import types


bot = telebot.TeleBot('token')

a: str = '' # a variable in which information is placed from which number system to which to translate


def check_2(number) -> bool:
    '''validation of a binary number'''
    for n in number.text:
        if n not in ['0', '1']:
            bot.send_message(number.chat.id, 'Это не двоичное число! Попробуй еще раз.')
            return False
    return True


def check_10(number) -> bool:
    '''validation of a decimal number'''
    for n in number.text:
        if n not in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
            bot.send_message(number.chat.id, 'Это не десятичное число! Попробуй еще раз.')
            return False
    return True


def check_8(number) -> bool:
    '''validation of an octal number'''
    for n in number.text:
        if n not in ['0', '1', '2', '3', '4', '5', '6', '7']:
            bot.send_message(number.chat.id, 'Это не восьмиричное число! Попробуй еще раз.')
            return False
    return True


def check_16(number) -> bool:
    '''validation of a hexadecimal number'''
    for n in number.text.upper():
        if n not in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']:
            bot.send_message(number.chat.id, 'Это не шестнадцатиричное число! Попробуй еще раз.')
            return False
    return True


def keyboard_help():
    '''function to create a help button'''
    help_kb = types.ReplyKeyboardMarkup(resize_keyboard=True)
    help_button = types.KeyboardButton(text="help")
    help_kb.add(help_button)
    return help_kb


def buttons():
    '''function to create a keyboard'''
    translate = types.InlineKeyboardMarkup(row_width=3)
    two_eight = types.InlineKeyboardButton(text='2 >>> 8', callback_data='2->8')
    two_ten = types.InlineKeyboardButton(text='2 >>> 10', callback_data='2->10')
    two_sixteen = types.InlineKeyboardButton(text='2 >>> 16', callback_data='2->16')
    ten_two = types.InlineKeyboardButton(text='10 >>> 2', callback_data='10->2')
    ten_eight = types.InlineKeyboardButton(text='10 >>> 8', callback_data='10->8')
    ten_sixteen = types.InlineKeyboardButton(text='10 >>> 16', callback_data='10->16')
    eight_two = types.InlineKeyboardButton(text='8 >>> 2', callback_data='8->2')
    eight_ten = types.InlineKeyboardButton(text='8 >>> 10', callback_data='8->10')
    eight_sixteen = types.InlineKeyboardButton(text='8 >>> 16', callback_data='8->16')
    sixteen_two = types.InlineKeyboardButton(text='16 >>> 2', callback_data='16->2')
    sixteen_eight = types.InlineKeyboardButton(text='16 >>> 8', callback_data='16->8')
    sixteen_ten = types.InlineKeyboardButton(text='16 >>> 10', callback_data='16->10')
    translate.add(two_eight, two_ten, two_sixteen,
                  ten_two, ten_eight, ten_sixteen,
                  eight_two, eight_ten, eight_sixteen,
                  sixteen_two, sixteen_eight, sixteen_ten)
    return translate


@bot.message_handler(regexp=r'\b[HhEeLliIOo]+\b')
@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, f'Hello, {message.from_user.username}!', reply_markup=keyboard_help())
    bot.send_message(message.chat.id, f'Translation keyboard', reply_markup=buttons())


@bot.message_handler(func= lambda m: m.text == 'help')
def helps(message):
    bot.send_message(message.chat.id, f'This bot is designed to translate numbers '
                                      f'from one number system to another number system. '
                                      f'Four number systems are available:'
                                      f'\n1. Binary\n2. Octal\n3. Hexadecimal\n4. Decimal'
                                      f'\n\n/start - starting command')
    bot.delete_message(message.chat.id, message.id)


@bot.callback_query_handler(func=lambda callback: callback.data)
def answer_call(callback):
    global a
    if callback.data == '2->10':
        bot.send_message(callback.message.chat.id, '(2 >>> 10) Введите двоичное число')
        a = '2->10'
        return a
    if callback.data == '2->8':
        a = '2->8'
        bot.send_message(callback.message.chat.id, '(2 >>> 8) Введите двоичное число')
        return a
    if callback.data == '2->16':
        bot.send_message(callback.message.chat.id, '(2 >>> 16) Введите двоичное число')
        a = '2->16'
        return a
    if callback.data == '10->2':
        bot.send_message(callback.message.chat.id, '(10 >>> 2) Введите десятичное число')
        a = '10->2'
        return a
    if callback.data == '10->8':
        bot.send_message(callback.message.chat.id, '(10 >>> 8) Введите десятичное число')
        a = '10->8'
        return a
    if callback.data == '10->16':
        bot.send_message(callback.message.chat.id, '(10 >>> 16) Введите десятичное число')
        a = '10->16'
        return a
    if callback.data == '8->2':
        bot.send_message(callback.message.chat.id, '(8 >>> 2) Введите восьмиричное число')
        a = '8->2'
        return a
    if callback.data == '8->10':
        bot.send_message(callback.message.chat.id, '(8 >>> 10) Введите восьмиричное число')
        a = '8->10'
        return a
    if callback.data == '8->16':
        bot.send_message(callback.message.chat.id, '(8 >>> 16) Введите восьмиричное число')
        a = '8->16'
        return a
    if callback.data == '16->2':
        bot.send_message(callback.message.chat.id, '(16 >>> 2) Введите шестнадцатиричное число')
        a = '16->2'
        return a
    if callback.data == '16->8':
        bot.send_message(callback.message.chat.id, '(16 >>> 8) Введите шестнадцатиричное число')
        a = '16->8'
        return a
    if callback.data == '16->10':
        bot.send_message(callback.message.chat.id, '(16 >>> 10) Введите шестнадцатиричное число')
        a = '16->10'
        return a


@bot.message_handler(regexp=r'\b[\d\w]+\b')
def two_tens(number):
    if a == '2->10' and check_2(number):
        bot.send_message(number.chat.id, f'Число в двоичной системе: <b>{number.text}</b>\n'
                                         f'Число в десятиричной системе: <b>{translator.func_exchange_2(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(2 >>> 10) Введите двоичное число')

    if a == '2->8' and check_2(number):
        bot.send_message(number.chat.id, f'Число в двоичной системе: <b>{number.text}</b>\n'
                                         f'Число в восьмиричной системе: <b>{translator.func_exchange_2(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(2 >>> 8) Введите двоичное число')

    if a == '2->16' and check_2(number):
        bot.send_message(number.chat.id, f'Число в двоичной системе: <b>{number.text}</b>\n'
                                         f'Число в шестнадцатиричной системе: <b>{translator.func_exchange_2(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(2 >>> 16) Введите двоичное число')

    if a == '10->2' and check_10(number):
        bot.send_message(number.chat.id, f'Число в десятиричной системе: <b>{number.text}</b>\n'
                                         f'Число в двоичной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(10 >>> 2) Введите десятичное число')

    if a == '10->8' and check_10(number):
        bot.send_message(number.chat.id, f'Число в десятиричной системе: <b>{number.text}</b>\n'
                                         f'Число в восьмиричной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(10 >>> 8) Введите десятичное число')

    if a == '10->16' and check_10(number):
        bot.send_message(number.chat.id, f'Число в десятиричной системе: <b>{number.text}</b>\n'
                                         f'Число в шестнадцатиричной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(10 >>> 16) Введите десятичное число')

    if a == '8->2' and check_8(number):
        bot.send_message(number.chat.id, f'Число в восьмиричной системе: <b>{number.text}</b>\n'
                                         f'Число в двоичной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(8 >>> 2) Введите восьмиричное число')

    if a == '8->10' and check_8(number):
        bot.send_message(number.chat.id, f'Число в восьмиричной системе: <b>{number.text}</b>\n'
                                         f'Число в десятиричной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(8 >>> 10) Введите восьмиричное число')

    if a == '8->16' and check_8(number):
        bot.send_message(number.chat.id, f'Число в восьмиричной системе: <b>{number.text}</b>\n'
                                         f'Число в шестнадцатиричной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(8 >>> 16) Введите восьмиричное число')

    if a == '16->2' and check_16(number):
        bot.send_message(number.chat.id, f'Число в шестнадцатиричной системе: <b>{number.text}</b>\n'
                                         f'Число в двоичной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(16 >>> 2) Введите шестнадцатиричное число')

    if a == '16->8' and check_16(number):
        bot.send_message(number.chat.id, f'Число в шестнадцатиричной системе: <b>{number.text}</b>\n'
                                         f'Число в восьмиричной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(16 >>> 8) Введите шестнадцатиричное число')

    if a == '16->10' and check_16(number):
        bot.send_message(number.chat.id, f'Число в шестнадцатиричной системе: <b>{number.text}</b>\n'
                                         f'Число в десятиричной системе: <b>{translator.func_exchange(number.text, a)}</b>',
                         parse_mode='HTML')
        bot.send_message(number.chat.id, 'Перевод систем счисления', reply_markup=buttons())
        bot.send_message(number.chat.id, '(16 >>> 10) Введите шестнадцатиричное число')


bot.polling()


